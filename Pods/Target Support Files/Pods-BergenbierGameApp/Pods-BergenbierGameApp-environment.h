
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// BSDatePickerWithPad
#define COCOAPODS_POD_AVAILABLE_BSDatePickerWithPad
#define COCOAPODS_VERSION_MAJOR_BSDatePickerWithPad 1
#define COCOAPODS_VERSION_MINOR_BSDatePickerWithPad 0
#define COCOAPODS_VERSION_PATCH_BSDatePickerWithPad 2

// BSNumPad
#define COCOAPODS_POD_AVAILABLE_BSNumPad
#define COCOAPODS_VERSION_MAJOR_BSNumPad 1
#define COCOAPODS_VERSION_MINOR_BSNumPad 0
#define COCOAPODS_VERSION_PATCH_BSNumPad 2

// DDHTimerControl
#define COCOAPODS_POD_AVAILABLE_DDHTimerControl
#define COCOAPODS_VERSION_MAJOR_DDHTimerControl 1
#define COCOAPODS_VERSION_MINOR_DDHTimerControl 0
#define COCOAPODS_VERSION_PATCH_DDHTimerControl 0

// DNDDragAndDrop
#define COCOAPODS_POD_AVAILABLE_DNDDragAndDrop
#define COCOAPODS_VERSION_MAJOR_DNDDragAndDrop 1
#define COCOAPODS_VERSION_MINOR_DNDDragAndDrop 1
#define COCOAPODS_VERSION_PATCH_DNDDragAndDrop 3

// NSDate+Helpers
#define COCOAPODS_POD_AVAILABLE_NSDate_Helpers
#define COCOAPODS_VERSION_MAJOR_NSDate_Helpers 1
#define COCOAPODS_VERSION_MINOR_NSDate_Helpers 2
#define COCOAPODS_VERSION_PATCH_NSDate_Helpers 0

// NuweScoreCharts
#define COCOAPODS_POD_AVAILABLE_NuweScoreCharts
#define COCOAPODS_VERSION_MAJOR_NuweScoreCharts 1
#define COCOAPODS_VERSION_MINOR_NuweScoreCharts 0
#define COCOAPODS_VERSION_PATCH_NuweScoreCharts 0

// TPKeyboardAvoiding
#define COCOAPODS_POD_AVAILABLE_TPKeyboardAvoiding
#define COCOAPODS_VERSION_MAJOR_TPKeyboardAvoiding 1
#define COCOAPODS_VERSION_MINOR_TPKeyboardAvoiding 2
#define COCOAPODS_VERSION_PATCH_TPKeyboardAvoiding 6

