//
//  ViewController.h
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DNDDragAndDrop/DNDDragAndDrop.h>

#import "DragView.h"



@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *dragSourceView;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *dropTargetViews;
@property (nonatomic, strong) IBOutlet DNDDragAndDropController *dragAndDropController;


@end

