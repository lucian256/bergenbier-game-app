//
//  Singleton.m
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "Singleton.h"
#import "AppDelegate.h"


const NSString * prize1 = @"prize1";
const NSString * prize2 = @"prize2";
const NSString * prize3 = @"prize3";

const NSString * isCora = @"cora";

const NSString * logData = @"logs";
const NSString * storeName = @"store";



@implementation Singleton
static Singleton *singletonInstance = nil;

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)])
    {
        context = [delegate managedObjectContext];
        
        
    }
    return context;
}

+ (id)getSingleton
{
    @synchronized(self)
    {
        if (singletonInstance == nil)
        {
            singletonInstance = [[super alloc] init];
            [singletonInstance runOnce];
        }
        return singletonInstance;
        
        
    }
}

- (void) runOnce
{
    
//    [singletonInstance getScreenMeasurements];
//    singletonInstance.barHeight =70;
//    
//    singletonInstance.points = 0;
//    singletonInstance.progress = 0;
//    
//    //
//    singletonInstance.swipeBackActive = true;
//    singletonInstance.screenLockCode  = @"1234";
//    //
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    //[defaults setObject:firstName forKey:@"firstName"];
//    //    NSString *firstName = [defaults objectForKey:@"firstName"];
//    singletonInstance.notificationsOn = [defaults boolForKey:@"NotificationsON"];
//    [singletonInstance updateNotifications];
}

#pragma Stoc
- (NSInteger) getPrize1
{
    NSInteger val = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    val = [defaults integerForKey:prize1];
    return  val;
}
- (void) setPrize1 : (NSInteger) val
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:val forKey:prize1];
    NSString * dataStore = [NSString stringWithFormat:@"%@ stoc: %ld",@"Prize1",(long)val];
    [self setLogs:dataStore];
}

- (NSInteger) getPrize2
{
    NSInteger val = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    val = [defaults integerForKey:prize2];
    return  val;
}
- (void) setPrize2 : (NSInteger) val
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:val forKey:prize2];
    NSString * dataStore = [NSString stringWithFormat:@"%@ stoc: %ld",@"Prize2",(long)val];
    [self setLogs:dataStore];
}

- (NSInteger) getPrize3
{
    NSInteger val = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    val = [defaults integerForKey:prize3];
    return  val;
}
- (void) setPrize3 : (NSInteger) val
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:val forKey:prize3];
    
    NSString * dataStore = [NSString stringWithFormat:@"%@ stoc: %ld",@"Prize3",(long)val];
    
    [self setLogs:dataStore];
}

- (BOOL) getIsCora
{
    NSInteger val = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    val = [defaults boolForKey:isCora];
    return  val;
}
- (void) setIsCora : (BOOL) val
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:val forKey:isCora];
}

- (NSString *) getStoreName
{
    NSString * val = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    val = [defaults stringForKey:storeName];
    
    if(val.length == 0 || val == nil)
    {
        return @"";
    }
    else
    {
        return  val;
    }
}
- (void) setStoreName : (NSString *) val
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:val forKey:storeName];
    
}


- (NSString *) getLogs
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * val = [defaults stringForKey:logData];
    return  val;
}
- (void) setLogs : (NSString *) val
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString * data = [self getLogs];
    
    //NSDate* now = [NSDate date];
    NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterFullStyle];

    
    NSString * dataStore = [NSString stringWithFormat:@"%@ \n %@ :-> %@  ",data,dateString,val];
    
    [defaults setObject:dataStore forKey:logData];
}


- (void) resetLogs
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:logData];
}

//- (void) currentPlayerDateOfBirth:(NSString *) dateofbirth withLastName: (NSString*) lastname withFirstName:(NSString*) firstname withAddress:(NSString*) address withEmail:(NSString*) email
//{
//    self.currentPlayer = [SelectedPlayer new];
//    
//    self.currentPlayer.datanasterii = dateofbirth;
//    self.currentPlayer.nume = lastname;
//    self.currentPlayer.prenume = firstname;
//    //self.currentPlayer.adresa = address;
//    self.currentPlayer.email = email;
//    
//    [self storeCurrentPlayerWithPrize:@"1"];
//}

-(void) setCurrentPlayerInfoWithDateOfBirth:(NSString *) dateofbirth WithLastName:(NSString *) lastname WithFirsName:(NSString *) firstname WithEmail:(NSString *) email WithPhone:(NSString *) phone WithBon:(NSString *) bon WithAttend:(NSString *) attend
{
    self.currentPlayer = [SelectedPlayer new];
    
    self.currentPlayer.datanasterii         = dateofbirth;
    self.currentPlayer.nume                 = lastname;
    self.currentPlayer.prenume              = firstname;
    self.currentPlayer.email                = email;
    self.currentPlayer.telefon              = phone;
    self.currentPlayer.numarbon             = bon;
    self.currentPlayer.participaextragere   = attend;
    
}


- (void) storeCurrentPlayerWithPrize:(NSString *) prize
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject *newContact;
    newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Player" inManagedObjectContext:context];
    
    [newContact setValue: self.currentPlayer.datanasterii forKey:@"datanasterii"];
    [newContact setValue: self.currentPlayer.nume forKey:@"nume"];
    [newContact setValue: self.currentPlayer.prenume forKey:@"prenume"];
    [newContact setValue: self.currentPlayer.email forKey:@"email"];
    [newContact setValue: self.currentPlayer.telefon forKey:@"telefon"];
    [newContact setValue: self.currentPlayer.numarbon forKey:@"numarbon"];
    [newContact setValue: self.currentPlayer.participaextragere forKey:@"participaextragere"];
    

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:SS"];
    NSString *stringDate = [dateFormatter stringFromDate:[NSDate date]];
    
    [newContact setValue: stringDate forKey:@"datacurenta"];
    
    if([prize isEqualToString:@"1"])
    {
        prize = @"Tricou";
    }
    if([prize isEqualToString:@"2"])
    {
        prize = @"Agenda";
    }
    if([prize isEqualToString:@"3"])
    {
        prize = @"Breloc";
    }
    
    [newContact setValue: prize forKey:@"premiu"];
    NSError *error;
    [context save:&error];
}

- (NSString *) getstoredPlayers
{
    NSString * result = [NSString stringWithFormat:@"  %@   \n",[self getStoreName]]; //@"";
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Player" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name = %@)", _name.text];
//    [request setPredicate:pred];
   // NSPredicate *pred = [NSPredicate predicateWithFormat:@""];
   // [request setPredicate:pred];
    NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if ([objects count] == 0)
    {
        result = [NSString stringWithFormat:@"%@ \n Nu exista date ! ",result];
        //NSLog(@"No records");
    }
    else
    {
        
        
        for(int i =0; i< [objects count];i++)
        {
            SelectedPlayer * player = [SelectedPlayer new];
            matches = objects[i];
            player.email    =[matches valueForKey:@"email"];
            player.nume     =[matches valueForKey:@"nume"];
            player.prenume  =[matches valueForKey:@"prenume"];
            player.telefon  =[matches valueForKey:@"telefon"];
            player.premiu   =[matches valueForKey:@"premiu"];
            player.datacurenta          =[matches valueForKey:@"datacurenta"];
            player.participaextragere   =[matches valueForKey:@"participaextragere"];

            result = [NSString stringWithFormat:@"%@ \n Jucator %d : Nume:%@ Prenume:%@ Email:%@ Telefon:%@ DataJoc:%@ Premiu:%@ Extragere:%@ ",result,i,player.nume,player.prenume,player.email,player.telefon,player.datacurenta,player.premiu,player.participaextragere];
        }
//        [newContact setValue: self.currentPlayer.adresa forKey:@"adresa"];
//        [newContact setValue: self.currentPlayer.email forKey:@"email"];
//        [newContact setValue: self.currentPlayer.dataNasterii forKey:@"datanasterii"];
//        [newContact setValue: self.currentPlayer.nume forKey:@"nume"];
//        [newContact setValue: self.currentPlayer.prenume forKey:@"prenume"];
//        [newContact setValue: self.currentPlayer.datacurenta forKey:@"datacurenta"];
//        [newContact setValue: self.currentPlayer.premiu forKey:@"premiu"];
//        matches = objects[0];
//        _address.text = [matches valueForKey:@"address"];
//        _phone.text = [matches valueForKey:@"phone"];
//        _status.text = [NSString stringWithFormat:
//                        @"%lu matches found", (unsigned long)[objects count]];
    }
    return result;
}

- (void)deleteAllEntities//:(NSString *)nameEntity
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Player"];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [context deleteObject:object];
    }
    
    error = nil;
    [context save:&error];
}

-(NSString *) getTodayWinners
{
    NSString * result = @"";
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Player" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    //    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name = %@)", _name.text];
    //    [request setPredicate:pred];
    // NSPredicate *pred = [NSPredicate predicateWithFormat:@""];
    // [request setPredicate:pred];
    NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if ([objects count] == 0)
    {
        NSLog(@"No records");
    }
    else
    {
        int valTricou = 0;
        int valAgenda = 0;
        int valBreloc = 0;
        
        for(int i =0; i< [objects count];i++)
        {
            SelectedPlayer * player = [SelectedPlayer new];
            matches = objects[i];
            player.datacurenta =[matches valueForKey:@"datacurenta"];
            player.premiu =[matches valueForKey:@"premiu"];
            player.nume =[matches valueForKey:@"nume"];
            
            
            if(![player.nume isEqualToString:@""])
            {
            NSDateFormatter *dateFormatter = [NSDateFormatter new];
            // this is imporant - we set our input date format to match our input string
            // if format doesn't match you'll get nil from your string, so be careful
            [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:SS"];
            NSDate *dateFromString = [[NSDate alloc] init];
            // voila!
            dateFromString = [dateFormatter dateFromString:player.datacurenta];
            NSDate *date = [NSDate date];
            
            NSInteger interval = [[[NSCalendar currentCalendar] components: NSDayCalendarUnit
                                                                  fromDate: dateFromString
                                                                    toDate: date
                                                                   options: 0] day];
            NSLog(@"%d %@",i,dateFromString);
            
            if(interval == 0)
            {
                if([player.premiu isEqualToString:@"Tricou"])
                {
                    valTricou ++;
                }
                if([player.premiu isEqualToString:@"Agenda"])
                {
                    valAgenda ++;
                }
                if([player.premiu isEqualToString:@"Breloc"])
                {
                    valBreloc ++;
                }
            }
            }
        }
        result = [NSString stringWithFormat:@" \n Astazi : Jocuri Totale : %d \n   \nCastiguri \n Tricouri : %d \n Agende : %d \n Brelocuri : %d",(valTricou + valBreloc+valAgenda),valTricou,valAgenda,valBreloc];
        
    }
    return result;
}
-(NSString *) getWinnersOnDate:(NSDate *) selectedDate
{
    NSString * result = @"";
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Player" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    //    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name = %@)", _name.text];
    //    [request setPredicate:pred];
    // NSPredicate *pred = [NSPredicate predicateWithFormat:@""];
    // [request setPredicate:pred];
    NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if ([objects count] == 0)
    {
        result = @"Nu sunt date";
        NSLog(@"No records");
    }
    else
    {
        int valTricou = 0;
        int valAgenda = 0;
        int valBreloc = 0;
        
        for(int i =0; i< [objects count];i++)
        {
            SelectedPlayer * player = [SelectedPlayer new];
            matches = objects[i];
            player.datacurenta =[matches valueForKey:@"datacurenta"];
            player.premiu =[matches valueForKey:@"premiu"];
            player.nume =[matches valueForKey:@"nume"];
            
            
            if(![player.nume isEqualToString:@""])
            {
                NSDateFormatter *dateFormatter = [NSDateFormatter new];
                // this is imporant - we set our input date format to match our input string
                // if format doesn't match you'll get nil from your string, so be careful
                [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:SS"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:player.datacurenta];
                NSDate *date = selectedDate;
                

                NSDateComponents *componentsC = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:dateFromString];
                int yearC = [componentsC year];
                int monthC = [componentsC month];
                int dayC = [componentsC day];
                NSDateComponents *componentsS = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:selectedDate];
                int yearS = [componentsS year];
                int monthS = [componentsS month];
                int dayS = [componentsS day];
                
//                NSInteger interval = [[[NSCalendar currentCalendar] components: NSDayCalendarUnit
//                                                                      fromDate: dateFromString
//                                                                        toDate: date
//                                                                       options: 0] day];
//                NSLog(@"%d %@",i,dateFromString);
                
                if(yearC == yearS && monthC == monthS && dayS == dayC)
                {
                    if([player.premiu isEqualToString:@"Tricou"])
                    {
                        valTricou ++;
                    }
                    if([player.premiu isEqualToString:@"Agenda"])
                    {
                        valAgenda ++;
                    }
                    if([player.premiu isEqualToString:@"Breloc"])
                    {
                        valBreloc ++;
                    }
                }
            }
        }
        result = [NSString stringWithFormat:@" \n Astazi : Jocuri Totale : %d \n   \nCastiguri \n Tricouri : %d \n Agende : %d \n Brelocuri : %d",(valTricou + valBreloc+valAgenda),valTricou,valAgenda,valBreloc];
        
    }
    return result;
}
-(NSString *) getAllWinners
{
    NSString * result = @"";
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Player" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    //    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name = %@)", _name.text];
    //    [request setPredicate:pred];
    // NSPredicate *pred = [NSPredicate predicateWithFormat:@""];
    // [request setPredicate:pred];
    NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if ([objects count] == 0)
    {
        result = @"Nu sunt date";
        NSLog(@"No records");
    }
    else
    {
        int valTricou = 0;
        int valAgenda = 0;
        int valBreloc = 0;
        
        for(int i =0; i< [objects count];i++)
        {
            SelectedPlayer * player = [SelectedPlayer new];
            matches = objects[i];
            player.datacurenta =[matches valueForKey:@"datacurenta"];
            player.premiu =[matches valueForKey:@"premiu"];
            player.nume =[matches valueForKey:@"nume"];
            
            
            if(![player.nume isEqualToString:@""])
            {
                    if([player.premiu isEqualToString:@"Tricou"])
                    {
                        valTricou ++;
                    }
                    if([player.premiu isEqualToString:@"Agenda"])
                    {
                        valAgenda ++;
                    }
                    if([player.premiu isEqualToString:@"Breloc"])
                    {
                        valBreloc ++;
                    }
               
            }
        }
        result = [NSString stringWithFormat:@" \n Astazi : Jocuri Totale : %d \n   \nCastiguri \n Tricouri : %d \n Agende : %d \n Brelocuri : %d",(valTricou + valBreloc+valAgenda),valTricou,valAgenda,valBreloc];
        
    }
    return result;
}

-(NSString *) createExcel
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Player" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    //    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name = %@)", _name.text];
    //    [request setPredicate:pred];
    // NSPredicate *pred = [NSPredicate predicateWithFormat:@""];
    // [request setPredicate:pred];
    NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
//    if ([objects count] == 0)
//    {
//        NSLog(@"No records");
//    }
//    else
//    {
//        NSLog(@"Records %lu", (unsigned long)[objects count]);
//        
//        
//        
//        for(int i =0; i< [objects count];i++)
//        {
//            SelectedPlayer * player = [SelectedPlayer new];
//            matches = objects[i];
//            player.adresa =[matches valueForKey:@"adresa"];
//            player.email  =[matches valueForKey:@"email"];
//            player.nume =[matches valueForKey:@"nume"];
//            player.prenume =[matches valueForKey:@"prenume"];
//            player.dataNasterii =[matches valueForKey:@"datanasterii"];
//            player.premiu =[matches valueForKey:@"premiu"];
//            player.datacurenta =[matches valueForKey:@"datacurenta"];
//            
//            //result = [NSString stringWithFormat:@"%@ \n Player %d: %@ %@ %@ %@ ",result,i,player.nume,player.prenume,player.datacurenta,player.premiu];
//        }
//    }
    
    
   // NSArray *firstArray, *secondArray, *thirdArray;
    NSMutableString *csv = [NSMutableString stringWithString:@"test1,test2,test3"];
    
//    NSUInteger count = [firstArray count];
//    // provided all arrays are of the same length
//    for (NSUInteger i=0; i<count; i++ )
//    {
//        [csv appendFormat:@"\n\"%@\",%@,\"%d\"",
//         [firstArray objectAtIndex:i],
//         [secondArray objectAtIndex:i],
//         [[thirdArray objectAtIndex:i] integerValue]
//         ];
//        // instead of integerValue may be used intValue or other, it depends how array was created
//    }
    for(int i =0; i< [objects count];i++)
    {
        SelectedPlayer * player = [SelectedPlayer new];
        matches = objects[i];
        //player.adresa =[matches valueForKey:@"adresa"];
        player.email  =[matches valueForKey:@"email"];
        player.nume =[matches valueForKey:@"nume"];
        player.prenume =[matches valueForKey:@"prenume"];
        player.datanasterii =[matches valueForKey:@"datanasterii"];
        player.premiu =[matches valueForKey:@"premiu"];
        player.datacurenta =[matches valueForKey:@"datacurenta"];
        
        [csv appendFormat:@"\n\"%@\",%@,\"%d\"",player.prenume, player.email, [player.premiu integerValue] ];
        
        //result = [NSString stringWithFormat:@"%@ \n Player %d: %@ %@ %@ %@ ",result,i,player.nume,player.prenume,player.datacurenta,player.premiu];
    }
    return csv;
    
    NSString *yourFileName = @"ResultXls";
    //NSError *error;
    BOOL res = [csv writeToFile:yourFileName atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    if (!res) {
        NSLog(@"Error %@ while writing to file %@", [error localizedDescription], yourFileName );
    }

}
-(BOOL) checkforNumeric:(NSString*) str
{
    NSString *strMatchstring=@"\\b([0-9%_.+\\-]+)\\b";
    NSPredicate *textpredicate=[NSPredicate predicateWithFormat:@"SELF MATCHES %@", strMatchstring];
    
    if(![textpredicate evaluateWithObject:str])
    {
        return FALSE;
    }
    return TRUE;
}

- (NSString *)getPrize
{
    //NSString * prize =@"";
    
    NSMutableArray *array  = [NSMutableArray new];
    
    if(self.getPrize1 > 0)
    {
        [array addObject:@"1"];
    }
    
    if(self.getPrize2 > 0)
    {
        [array addObject:@"2"];
    }
    
    if(self.getPrize3 > 0)
    {
        [array addObject:@"3"];
    }
    
    if ([array count] == 0)
    {
        return @"-1";
    }
    
    NSInteger index = arc4random() % [array count];
    NSString * result = [array objectAtIndex: index ];
    
    if([result isEqualToString:@"1"])
    {
        NSInteger val = self.getPrize1 -1;
        [self setPrize1:val];
    }
    
    if([result isEqualToString:@"2"])
    {
        NSInteger val = self.getPrize2 -1;
        [self setPrize2:val];
    }
    
    if([result isEqualToString:@"3"])
    {
        NSInteger val = self.getPrize3 -1;
        [self setPrize3:val];
    }
    
    [self storeCurrentPlayerWithPrize:result];
    return result;
    
    
    
}


@end
