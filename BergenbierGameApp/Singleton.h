//
//  Singleton.h
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "SelectedPlayer.h"


@interface Singleton : NSObject

@property(strong, atomic) SelectedPlayer * currentPlayer;
@property( atomic ) bool masterAdmin;


#pragma Singleton
+ (id)getSingleton;

#pragma CoreData
//old
//- (void) currentPlayerDateOfBirth:(NSString *) dateofbirth withLastName: (NSString*) lastname withFirstName:(NSString*) firstname withAddress:(NSString*) address withEmail:(NSString*) email;



-(void) setCurrentPlayerInfoWithDateOfBirth:(NSString *) dateofbirth WithLastName:(NSString *) lastname WithFirsName:(NSString *) firstname WithEmail:(NSString *) email WithPhone:(NSString *) phone WithBon:(NSString *) bon WithAttend:(NSString *) attend;

- (void) storeCurrentPlayerWithPrize:(NSString *) prize;

- (NSString *) getstoredPlayers;
- (NSString *) getTodayWinners;
- (NSString *) getWinnersOnDate:(NSDate *) selectedDate;
- (NSString *) getAllWinners;
- (NSString *) createExcel;

#pragma Stoc
- (NSInteger) getPrize1;
- (void) setPrize1 : (NSInteger) val;
- (NSInteger) getPrize2;
- (void) setPrize2 : (NSInteger) val;
- (NSInteger) getPrize3;
- (void) setPrize3 : (NSInteger) val;

- (NSString *) getLogs;

- (NSString *) getStoreName;
- (void) setStoreName : (NSString *) val;


- (BOOL) checkforNumeric:(NSString*) str;

- (NSString *)getPrize;

- (BOOL) getIsCora;
- (void) setIsCora : (BOOL) val;

- (void)deleteAllEntities;

@end
