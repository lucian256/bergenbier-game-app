//
//  GameViewController.m
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "GameViewController.h"
#import <QuartzCore/QuartzCore.h>





@interface GameViewController () <DNDDragSourceDelegate, DNDDropTargetDelegate, RZTweenAnimatorDelegate>
@end


@implementation GameViewController
Singleton * singletonGame;

int count = 21;
int countViews = 21;
- (void)viewDidLoad
{
    [super viewDidLoad];
    singletonGame = [Singleton getSingleton];
    //Drag
    self.dragAndDropController = [[DNDDragAndDropController alloc] init];
    [self.dragAndDropController registerDragSource:self.dragSourceView withDelegate:self];
    for (UIView *dropTargetView in self.dropTargetViews)
    {
        [self.dragAndDropController registerDropTarget:dropTargetView withDelegate:self];
    }
    [self updateStash];
    
    self.firstViewAnimator = [[RZTweenAnimator alloc] init];
    self.firstViewAnimator.delegate = self;
    float  val = self.Constraint_supTop.constant;
    RZFloatTween *moveTween = [[RZFloatTween alloc]initWithCurveType:RZTweenCurveTypeQuadraticEaseOut];
    [moveTween addKeyFloat:val atTime:0.0f];
    [moveTween addKeyFloat:val-20 atTime:1.0f];
    [moveTween addKeyFloat:val atTime:2.0f];
    [self.firstViewAnimator addTween:moveTween forKeyPath:@"constant" ofObject:self.Constraint_supTop];
    
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString *path = [NSString stringWithFormat:@"%@/crowd2.mp3", resourcePath];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:&error];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addClock2];
    self.gameEnd = false;
    //count = 21;
    count  = self.dropTargetViews.count;
    countViews = self.dropTargetViews.count;
    [self updateStash];
    self.ViewPrize.hidden = true;
    //[self addClock2];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma playSound
-(void) playSound
{

//    self.audioPlayer.numberOfLoops = 0;
//    self.audioPlayer.volume = 1.0f;
    [self.audioPlayer play];
}

#pragma mark - Drag Source Delegate
- (UIView *)draggingViewForDragOperation:(DNDDragOperation *)operation
{
    UIView *dragView = [[DragView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 60.0f, 80.0f)];
    dragView.alpha = 0.0f;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.alpha = 1.0f;
    }];
    count --;
    [self updateStash];
    
    
    
    return dragView;
}

- (void)dragOperationWillCancel:(DNDDragOperation *)operation
{
    // Drag operation will cancel
    [operation removeDraggingViewAnimatedWithDuration:0.2 animations:^(UIView *draggingView)
    {
        draggingView.alpha = 0.0f;
        draggingView.center = [operation convertPoint:self.dragSourceView.center fromView:self.view];
        count ++;
        [self updateStash];
    }];
}


#pragma mark - Drop Target Delegate

- (void)dragOperation:(DNDDragOperation *)operation didDropInDropTarget:(UIView *)target
{
    //Drop End
    target.backgroundColor = operation.draggingView.backgroundColor;
    target.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    NSString *name =@"";
    name = [NSString stringWithFormat:@"Sup_%ld",(target.tag-1)];
    
    UIImageView *imageView = (UIImageView*)[target viewWithTag:1];
    imageView.image = [UIImage imageNamed:name];
    
    [self.dragAndDropController unregisterDropTarget:target];
    self.audioPlayer.volume = 1.0f;
    countViews --;
    
    [self animSup];
    
    [self endGame];

}

-(void) endGame
{
    if(self.gameEnd)
    {
        return;
    }
    
    if(countViews  == 0 || !self.circularTimer.isRunning)
    {
        [self.audioPlayer stop];
        
        [self.circularTimer stop];
        
        NSString* prize = [singletonGame getPrize];
        [self.view bringSubviewToFront:self.ViewPrize];
        self.ViewPrize.hidden = false;
        NSString * prizeString = [NSString stringWithFormat:@"Prize%@",prize ];
        
        self.ImageView_Prize.image = [UIImage imageNamed:prizeString];
        
        //[self showAlertWithText:@"GameDone"];
        self.gameEnd = true;
    }
}

bool animIsRunning = false;
-(void) animSup
{
    animIsRunning = !animIsRunning;
    [self.firstViewAnimator setTime:0.0f];
    [self.firstViewAnimator animateToTime:3.0f];

    //[self playSound];
    
//    [UIView animateWithDuration:2.0 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        if(animIsRunning)
//        {
//            self.Constraint_supTop.constant = 100;
//        }
//        else
//        {
//            self.Constraint_supTop.constant = 130;
//        }
//    } completion:^(BOOL finished) {
//        
//    }];
    
//    [UIView animateWithDuration:1.0
//                        delay: 0.0
//                        options: UIViewAnimationOptionCurveLinear
//                        animations:^{
//        
//    }];
}

- (void)dragOperation:(DNDDragOperation *)operation didEnterDropTarget:(UIView *)target
{
    //Drop Hover
    //target.layer.borderColor = [operation.draggingView.backgroundColor CGColor];
}

- (void)dragOperation:(DNDDragOperation *)operation didLeaveDropTarget:(UIView *)target
{
    //Drop Hover
    //target.layer.borderColor = [[UIColor whiteColor] CGColor];
}

#pragma Stuff

- (IBAction)Action_Button_Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) updateStash
{
    UIImageView *imageView = (UIImageView*)[self.dragSourceView viewWithTag:1];
    
    NSString *name =@"";
    name = [NSString stringWithFormat:@"Stock_%d",count];
    imageView.image = [UIImage imageNamed:name];
}

//-(void) addClock
//{
//    self.timerControl = [DDHTimerControl timerControlWithType:DDHTimerTypeSolid];
//    self.timerControl.translatesAutoresizingMaskIntoConstraints = NO;
//    self.timerControl.color = [UIColor orangeColor];
//    self.timerControl.titleLabel.textColor = self.timerControl.color;
//    self.timerControl.highlightColor = [UIColor redColor];
//    self.timerControl.minutesOrSeconds = 30;
//    self.timerControl.maxValue = self.timerControl.minutesOrSeconds;
//    
//    self.timerControl.titleLabel.text = @"sec";
//    self.timerControl.userInteractionEnabled = NO;
//    self.timerControl.ringWidth = 10.0f;
//    
//    
//    self.timerControl.frame =CGRectMake(0,0,self.View_Clock.frame.size.width,self.View_Clock.frame.size.height);
//
//    [self.View_Clock addSubview:self.timerControl];
//    
//    
//    //self.View_Clock.transform = CGAffineTransformMakeRotation(2*M_PI_2);
//    
////    [self.view addSubview:self.timerControl];
////    self.View_Clock.hidden = YES;
//    
//    [self.timerControl addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
//    
//    [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(changeTimer:) userInfo:nil repeats:YES];
//    self.endDate = [NSDate dateWithTimeIntervalSinceNow:self.timerControl.minutesOrSeconds];
//}
//
//- (void)valueChanged:(DDHTimerControl*)sender
//{
//    //NSLog(@"value: %d", sender.minutesOrSeconds);
//}
//
//- (void)changeTimer:(NSTimer*)timer
//{
//    NSTimeInterval timeInterval = [self.endDate timeIntervalSinceNow];
//    //NSLog(@"timeInterval: %f, minutes: %f", timeInterval, timeInterval/60.0f);
//    if(timeInterval <= 0)
//    {
//        [timer invalidate];
//        timer = nil;
//    }
//    self.timerControl.minutesOrSeconds = (NSInteger)(timeInterval);
//    
//    if(timeInterval < 10)
//    {
//        self.timerControl.color = [UIColor redColor];
//        self.timerControl.titleLabel.textColor = self.timerControl.color;
//    }
//}
////

- (void) addClock2
{
    //float radius = 10 ;
    float internalRadius = 30;
    
    UIColor * backgroundColor       = [UIColor yellowColor];
    UIColor * backgroundFadeColor   = [UIColor redColor];
    UIColor * foregroundColor       = [UIColor colorWithRed:221/255.0 green:223/255.0 blue:227/255.0 alpha:1];;
    UIColor * foregroundFadeColor   = foregroundColor;//[UIColor blueColor];
    NSInteger direction             = 1;
    NSTimeInterval countdownSeconds = 30;
    
    
    //self.circularTimer = [[CircularTimerView alloc] initWithPosition:CGPointMake(10.0f, 10.0f) radius:radius internalRadius:internalRadius];
    CGRect frame = CGRectMake(2,2,self.View_Clock.frame.size.width-4,self.View_Clock.frame.size.height-4);
    self.circularTimer = [[CircularTimerView alloc] initWithFrame:frame];
    //self.circularTimer.radius = radius;
    self.circularTimer.internalRadius = internalRadius;
    self.circularTimer.backgroundColor = backgroundColor;
    self.circularTimer.backgroundFadeColor = backgroundFadeColor;
    self.circularTimer.foregroundColor = foregroundColor;
    self.circularTimer.foregroundFadeColor = foregroundFadeColor;
    self.circularTimer.direction = direction;
    self.circularTimer.font = [UIFont boldSystemFontOfSize:110];//[UIFont systemFontOfSize:92];
    self.circularTimer.fontColor = [UIColor orangeColor];
    
    self.circularTimer.backgroundImage = [UIImage imageNamed:@"BackgroundClock2"];
    
    //[self.circularTimer customBackgroundImage];
    __weak typeof(self) weakSelf = self;
    self.circularTimer.frameBlock = ^(CircularTimerView *circularTimerView)
    {
        circularTimerView.text =[NSString stringWithFormat:@"%.f",countdownSeconds - circularTimerView.runningElapsedTime];
        
        if(weakSelf.audioPlayer.volume < 0.5)
        {
            weakSelf.audioPlayer.volume += 0.05f;
        }
        
        if(weakSelf.audioPlayer.volume > 0.5)
        {
            weakSelf.audioPlayer.volume -= 0.05f;
        }
        
        [weakSelf endGame];
        
        //NSLog(@"frame");
//        circularTimerView.text =[NSString stringWithFormat:@"%.02f", circularTimerView.runningElapsedTime];
        //circularTimerView.text = [NSString stringWithFormat:@"%f", [circularTimerView intervalLength]];
    };
    
    [self.circularTimer setupCountdown:countdownSeconds];
    
    //__weak typeof(self) weakSelf = self;
    self.circularTimer.startBlock = ^(CircularTimerView *circularTimerView)
    {
        weakSelf.audioPlayer.volume = 0;
        [weakSelf playSound];
        NSLog(@"Running!");
        //weakSelf.statusLabel.text = @"Running!";
    };
    self.circularTimer.endBlock = ^(CircularTimerView *circularTimerView)
    {
        [weakSelf endGame];
         NSLog(@"Not running anymore!");
        //weakSelf.statusLabel.text = @"Not running anymore!";
    };
    //self.statusLabel.text = ([self.circularTimer willRun]) ? @"Circle will run" : @"Circle won't run";
    
    [self.View_Clock addSubview:self.circularTimer];
}

-(void) showAlertWithText:(NSString *) msg
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alerta!"
                                                   message: msg
                                                  delegate: self
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (IBAction)Action_Button_Restart:(id)sender
{
    
     [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
