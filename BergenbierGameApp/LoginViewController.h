//
//  LoginViewController.h
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *TextField_DataNasterii;
@property (strong, nonatomic) IBOutlet UITextField *TextField_Adresa;

- (IBAction)Action_Button_Game:(id)sender;
- (IBAction)Action_Button_Admin:(id)sender;

//An
@property (strong, nonatomic) IBOutlet UIView *View_DataNasteriiZi;
@property (strong, nonatomic) IBOutlet UITextField *TextField_DataNasteriiZi;
@property (strong, nonatomic) IBOutlet UIView *View_DataNasteriiLuna;
@property (strong, nonatomic) IBOutlet UITextField *TextField_DataNasteriiLuna;
@property (strong, nonatomic) IBOutlet UIView *View_DataNasteriiAn;
@property (strong, nonatomic) IBOutlet UITextField *TextField_DataNasteriiAn;

//Nume
@property (strong, nonatomic) IBOutlet UITextField *TextField_Nume;
@property (strong, nonatomic) IBOutlet UIView *View_Nume;
//Prenume
@property (strong, nonatomic) IBOutlet UITextField *TextField_Prenume;
@property (strong, nonatomic) IBOutlet UIView *View_Prenume;
//Email
@property (strong, nonatomic) IBOutlet UITextField *TextField_Email;
@property (strong, nonatomic) IBOutlet UIView *View_Email;
//Telefon
@property (strong, nonatomic) IBOutlet UITextField *TextField_Telefon;
@property (strong, nonatomic) IBOutlet UIView *View_Telefon;
//Bon
@property (strong, nonatomic) IBOutlet UITextField *TextField_Bon;
@property (strong, nonatomic) IBOutlet UIView *View_bon;


- (IBAction)Action_Button_DeleteYear:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *Button_Attend;
- (IBAction)Action_Button_Attend:(id)sender;

//Info
@property (strong, nonatomic) IBOutlet UIView *View_Contain;
@property (strong, nonatomic) IBOutlet UIImageView *ImageView_Info;
@property (strong, nonatomic) IBOutlet UIView *View_ContainInfo;
- (IBAction)Action_Button_StartGame:(id)sender;


@end
