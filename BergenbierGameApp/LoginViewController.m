//
//  LoginViewController.m
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "LoginViewController.h"

#import "Singleton.h"

#import <QuartzCore/QuartzCore.h>

@implementation LoginViewController

Singleton * singletonLogin;
bool attend = false;

UITapGestureRecognizer *tap;

- (void)viewDidLoad
{
    [super viewDidLoad];

    singletonLogin = [Singleton getSingleton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupViews];
    
    [self clearFields];
    
    singletonLogin.currentPlayer = [SelectedPlayer new];
    singletonLogin.masterAdmin = false;
    
}

float cornerLogin = 25;

-(void) setupViews
{
    self.View_DataNasteriiAn.layer.cornerRadius    = cornerLogin;
    self.View_DataNasteriiAn.layer.masksToBounds   = YES;
    self.View_DataNasteriiLuna.layer.cornerRadius    = cornerLogin;
    self.View_DataNasteriiLuna.layer.masksToBounds   = YES;
    self.View_DataNasteriiZi.layer.cornerRadius    = cornerLogin;
    self.View_DataNasteriiZi.layer.masksToBounds   = YES;
    self.View_Nume.layer.cornerRadius    = cornerLogin;
    self.View_Nume.layer.masksToBounds   = YES;
    self.View_Prenume.layer.cornerRadius    = cornerLogin;
    self.View_Prenume.layer.masksToBounds   = YES;
    self.View_Email.layer.cornerRadius    = cornerLogin;
    self.View_Email.layer.masksToBounds   = YES;
    self.View_Telefon.layer.cornerRadius    = cornerLogin;
    self.View_Telefon.layer.masksToBounds   = YES;
    self.View_bon.layer.cornerRadius    = cornerLogin;
    self.View_bon.layer.masksToBounds   = YES;
    
    self.TextField_DataNasteriiAn.delegate = self;
    self.TextField_DataNasteriiLuna.delegate = self;
    self.TextField_DataNasteriiZi.delegate = self;
    

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    int size = 100;
    if(textField.tag == 12)
    {
        size = 2;
    }
    if(textField.tag == 14)
    {
        size = 4;
    }
    if (textField.text.length >= size &&  string.length != 0)
    {
        return NO;
    }
    return YES;
}

- (void) clearFields
{
    self.TextField_DataNasteriiAn.text = @"";
    self.TextField_DataNasteriiLuna.text = @"";
    self.TextField_DataNasteriiZi.text = @"";
    self.TextField_Nume.text = @"";
    self.TextField_Prenume.text = @"";
    self.TextField_Telefon.text = @"";
    self.TextField_Bon.text = @"";
    self.TextField_Email.text = @"";
    
    attend = false;
    
    [self.Button_Attend setImage:[UIImage imageNamed:@"Box_Cancel"] forState:UIControlStateNormal];
    
    self.View_ContainInfo.hidden = YES;
    [self.View_Contain sendSubviewToBack:self.View_ContainInfo];
    
}

- (IBAction)Action_Button_Game:(id)sender
{
    [self.view endEditing:YES];
    [self validateData];
//    //Carrefour - PrizeInfo1
//    //Cora -PrizeInfo2
//    if([singletonLogin getIsCora])
//    {
//        self.ImageView_Info.image = [UIImage imageNamed:@"PrizeInfo2"];
//    }
//    else
//    {
//        self.ImageView_Info.image = [UIImage imageNamed:@"PrizeInfo1"];
//    }
//    
//    self.View_ContainInfo.hidden = NO;
//    [self.View_Contain bringSubviewToFront:self.View_ContainInfo];
    //[self performSegueWithIdentifier:@"login-game" sender:self];
    //[singleton currentPlayerDateOfBirth:@"P1" withLastName:@"P2" withFirstName:@"P3" withAddress:@"P4" withEmail:@"P5"];
}

- (IBAction)Action_Button_Admin:(id)sender
{
    if([self.TextField_DataNasteriiZi.text isEqualToString:@"1"] && [self.TextField_DataNasteriiLuna.text isEqualToString:@"2"] && [self.TextField_DataNasteriiAn.text isEqualToString:@"3"] )
    {
        singletonLogin.masterAdmin = false;
        
        [self performSegueWithIdentifier:@"login-admin" sender:self];
    }
    
    if([self.TextField_DataNasteriiZi.text isEqualToString:@"3"] && [self.TextField_DataNasteriiLuna.text isEqualToString:@"2"] && [self.TextField_DataNasteriiAn.text isEqualToString:@"1"] && [self.TextField_Nume.text isEqualToString:@"0"] )
    {
        singletonLogin.masterAdmin = true;
        
        [self performSegueWithIdentifier:@"login-admin" sender:self];
    }
}

- (void) showDatePicker
{
    
    
}

- (void) validateData
{
    if([self validateDataNasterii] && [self validateNume] && [self validatePrenume] && [self validateEmail] && [self validateTelefon] && [self validateBon])
    {
        if(singletonLogin.getPrize1 <= 0 && singletonLogin.getPrize2 <= 0 && singletonLogin.getPrize3 <= 0 )
        {
            eroare =@"Nu mai sunt premii!";
            [self showAlertWithText:@"Nu mai sunt premii!"];
            
            
        }
        else
        {
            //Carrefour - PrizeInfo1
            //Cora -PrizeInfo2
            if([singletonLogin getIsCora])
            {
                self.ImageView_Info.image = [UIImage imageNamed:@"PrizeInfo2"];
            }
            else
            {
                self.ImageView_Info.image = [UIImage imageNamed:@"PrizeInfo3"];
            }
            
            self.View_ContainInfo.hidden = NO;
            [self.View_Contain bringSubviewToFront:self.View_ContainInfo];
            
            NSString * birth = [NSString stringWithFormat:@"%@:%@:%@",self.TextField_DataNasteriiZi.text,self.TextField_DataNasteriiLuna.text,self.TextField_DataNasteriiAn.text ];
            NSString * lastname =self.TextField_Nume.text;
            NSString * firstname =self.TextField_Prenume.text;
            NSString * email =self.TextField_Email.text;
            NSString * phone =self.TextField_Telefon.text;
            NSString * bon =self.TextField_Bon.text;
            
            
            NSString * attendString =@"";
            
            if(attend)
            {
                attendString =@"Da";
            }
            else
            {
                attendString =@"Nu";
            }
            
            [singletonLogin setCurrentPlayerInfoWithDateOfBirth:birth WithLastName:lastname WithFirsName:firstname WithEmail:email WithPhone:phone WithBon:bon WithAttend:attendString];
            
        }
    }
    else
    {
        
        
        [self showAlertWithText:@"Date incorecte"];
    }
}


NSString * eroare=@"";

-(bool)validateDataNasterii
{
    bool valid = false;
    NSString * an = self.TextField_DataNasteriiAn.text;
    NSString * luna = self.TextField_DataNasteriiLuna.text;
    NSString * zi = self.TextField_DataNasteriiZi.text;
    
    int valAn = [an integerValue];
    int valLuna = [luna integerValue];
    int valZi = [zi integerValue];
    
    if([self dateExistsYear:valAn month:valLuna day:valZi])
    {
        valid = true;
    }
    if(!valid)
    {
       eroare =@"Data Nasterii";
    }
    
    return valid;
}
-(bool)validateNume
{
    bool valid = false;
    if(self.TextField_Nume.text.length >0)
    {
        valid = true;
    }
    
    if(!valid)
    {
        eroare =@"Nume";
    }
    
    return valid;
}
-(bool)validatePrenume
{
    bool valid = false;
    if(self.TextField_Prenume.text.length >0)
    {
        valid = true;
    }
    
    if(!valid)
    {
        eroare =@"Prenume";
    }
    
    return valid;
}

-(BOOL) validateBon
{
    bool valid = false;
    if(self.TextField_Bon.text.length >0)
    {
        if([singletonLogin checkforNumeric:self.TextField_Bon.text])
        valid = true;
    }
    
    if(!valid)
    {
        eroare =@"Bon";
    }
    
    return valid;
}
//-(bool)validateAdresa
//{
//    bool valid = false;
//    if(self.TextField_Adresa.text.length >0)
//    {
//        valid = true;
//    }
//    return valid;
//}
-(bool)validateEmail
{
    if(attend)
    {
        bool valid = false;
//        if(self.TextField_Email.text.length >0)
//        {
//            valid = true;
//        }
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        
        valid = [emailTest evaluateWithObject:self.TextField_Email.text];
        
        
        if(!valid)
        {
            eroare =@"Email";
        }
        
        return valid;
    }
    else
    {
        return true;
    }
}
-(BOOL) validateTelefon
{
    if(attend)
    {
        bool valid = false;
        if(self.TextField_Telefon.text.length >9)
        {
            if([singletonLogin checkforNumeric:self.TextField_Telefon.text])
                valid = true;
        }
        
        if(!valid)
        {
            eroare =@"Telefon";
        }
        return valid;
//        NSString *stringToBeTested = self.TextField_Telefon.text;
//        NSString *mobileNumberPattern = @"[0-9]";
//        NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
//        BOOL matched = [mobileNumberPred evaluateWithObject:stringToBeTested];
//        
//        if(!matched)
//        {
//            eroare =@"Telefon";
//        }
//        
//        return matched;
    }
    else
    {
        return true;
    }
}
- (IBAction)Action_Button_DeleteYear:(id)sender
{
    self.TextField_DataNasteriiAn.text = @"";
    self.TextField_DataNasteriiLuna.text = @"";
    self.TextField_DataNasteriiZi.text = @"";
}

- (IBAction)Action_Button_Attend:(id)sender
{
    if(attend)
    {
        attend = false;
        singletonLogin.currentPlayer.participaextragere = @"0";
        [self.Button_Attend setImage:[UIImage imageNamed:@"Box_Cancel"] forState:UIControlStateNormal];
    }
    else
    {
        attend = true;
        singletonLogin.currentPlayer.participaextragere = @"1";
        [self.Button_Attend setImage:[UIImage imageNamed:@"Box_Ok"] forState:UIControlStateNormal];
    }
}
//
- (BOOL)dateExistsYear:(int)year month:(int)month day:(int)day
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    dateFormatter.dateFormat = @"yyyyMMdd";
    NSString *dayString = [NSString stringWithFormat:@"%d",day];
    if(day<10)
    {
        dayString = [NSString stringWithFormat:@"0%d",day];
    }
    
    NSString *monthString = [NSString stringWithFormat:@"%d",month];
    if(month<10)
    {
        monthString = [NSString stringWithFormat:@"0%d",month];
    }
    
    
    NSString* inputString = [NSString stringWithFormat:@"%4d%@%@",
                             year,monthString,dayString];
    
    NSDate *date = [dateFormatter dateFromString:inputString];
    
    if(date == nil)
        return false;
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:date
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    
    if(age > 18)
    {
        return true;
    }
    else
    {
        return false;
    }
    //return nil != date;
}

//
-(void) showAlertWithText:(NSString *) msg
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alerta!"
                                                   message: eroare
                                                  delegate: self
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (IBAction)Action_Button_StartGame:(id)sender
{
    [self performSegueWithIdentifier:@"login-game" sender:self];
}
@end
