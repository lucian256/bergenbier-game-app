//
//  AdminViewController.m
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "AdminViewController.h"
#import <MessageUI/MessageUI.h>

@interface AdminViewController () <MFMailComposeViewControllerDelegate>
//@property (nonatomic, strong) NSArray *files;
@end

@implementation AdminViewController

Singleton * singletonAdmin;

- (void)viewDidLoad
{
    [super viewDidLoad];
    singletonAdmin = [Singleton getSingleton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getStock];
    
    self.Switch_isCora.on = [singletonAdmin getIsCora];
    
    NSString * result =[singletonAdmin getstoredPlayers];
    self.TextView_Output.text = result;
    self.DatePicker_Date.date = [NSDate date];
    
    self.Button_Reset.hidden = !singletonAdmin.masterAdmin;
    
    self.TextField_Prize1.hidden = !singletonAdmin.masterAdmin;
    self.TextField_Prize2.hidden = !singletonAdmin.masterAdmin;
    self.TextField_Prize3.hidden = !singletonAdmin.masterAdmin;
    
    self.Switch_isCora.hidden = !singletonAdmin.masterAdmin;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yy";
    
    NSDate * maxDate = [dateFormatter dateFromString:@"30-07-2015"];
    NSDate * minDate = [dateFormatter dateFromString:@"01-07-2015"];
    
    
    self.DatePicker_Date.maximumDate = maxDate;
    self.DatePicker_Date.minimumDate = minDate;
    
    self.TextField_NumeMagazin.text = [singletonAdmin getStoreName];
    

}

- (IBAction)Action_Button_BackSave:(id)sender
{
    if([self validateData])
    {
        if(singletonAdmin.masterAdmin)
        {
            [self saveStock];
        }
        
        [singletonAdmin setStoreName:self.TextField_NumeMagazin.text];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self alert];
    }
    
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)Action_Button_Email:(id)sender
{
    [singletonAdmin setStoreName:self.TextField_NumeMagazin.text];
    [self createFile];
}

- (BOOL) validateData
{
    bool isValid = true;
    
    NSString * val1 = self.TextField_Prize1.text;
    if(val1.length == 0 || ![singletonAdmin checkforNumeric:val1] || [self.TextField_Prize1.text integerValue] < 0)
    {
            isValid = false;
    }
    
    NSString * val2 = self.TextField_Prize2.text;
    if(val2.length == 0 || ![singletonAdmin checkforNumeric:val2] || [self.TextField_Prize2.text integerValue] < 0)
    {
        isValid = false;
    }
    
     NSString * val3 = self.TextField_Prize3.text;
    if(val3.length == 0 || ![singletonAdmin checkforNumeric:val3] || [self.TextField_Prize3.text integerValue] < 0)
    {
        isValid = false;
    }
    
    return isValid;
}

- (void) saveStock
{
    NSInteger val1 = [self.TextField_Prize1.text integerValue];
    [singletonAdmin setPrize1:val1];
    NSInteger val2 = [self.TextField_Prize2.text integerValue];
    [singletonAdmin setPrize2:val2];
    NSInteger val3 = [self.TextField_Prize3.text integerValue];
    [singletonAdmin setPrize3:val3];
}

- (void) getStock
{
    NSString * val1 = [NSString stringWithFormat:@"%ld",(long)singletonAdmin.getPrize1 ];
    self.TextField_Prize1.text = val1;
    NSString * val2 = [NSString stringWithFormat:@"%ld",(long)singletonAdmin.getPrize2 ];
    self.TextField_Prize2.text = val2;
    NSString * val3 = [NSString stringWithFormat:@"%ld",(long)singletonAdmin.getPrize3 ];
    self.TextField_Prize3.text = val3;
}


- (void) alert
{
    UIAlertView *objAlert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Datele introduse nu sunt valide!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close",nil];
    [objAlert show];
}

-(void) createFile
{
    
    NSString * result =[singletonAdmin getstoredPlayers];
    
    // Build the path, and create if needed.
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = [NSString stringWithFormat:@"Rezultate_%@.txt",singletonAdmin.getStoreName];//@"Result.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    
    // The main act...
    [[result dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    //EMAIL
    NSArray *toRecipents = [NSArray arrayWithObject:@"victor.furtuna@mediascope.ro"];
    MFMailComposeViewController *mail=[[MFMailComposeViewController alloc]init];
    
    if(mail == nil)
    {
        UIAlertView *objAlert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Nu este nici o adresa de email asociata acestui Device! Adaugati un cont de mail. Settings -> Mail-> Add Account" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close",nil];
        [objAlert show];
        return;
    }
    
    
    mail.mailComposeDelegate=self;
    
    NSString * emailTitle = [NSString stringWithFormat:@"Rezultate pentru %@",singletonAdmin.getStoreName];
    [mail setSubject:emailTitle];//@"Email with attached txt"];
    [mail setToRecipients:toRecipents];
    NSString *newFilePath = fileAtPath;
    
    NSData * pdfData = [NSData dataWithContentsOfFile:newFilePath];
    
    //[mail addAttachmentData:pdfData mimeType:@"application/x-excel" fileName:fileName];
    [mail addAttachmentData:pdfData mimeType:@"text/htm" fileName:fileName];
    NSString * body = @"";
    [mail setMessageBody:body isHTML:NO];
    [self presentModalViewController:mail animated:YES];
//    [mail release];
    
    
    
    //SEND email
//    NSString *emailTitle = @"Great Photo and Doc";
//    NSString *messageBody = @"Hey, check this out!";
//    NSArray *toRecipents = [NSArray arrayWithObject:@"alexandru@adonissoft.com"];
//    
//    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//    mc.mailComposeDelegate = self;
//    [mc setSubject:emailTitle];
//    [mc setMessageBody:messageBody isHTML:NO];
//    [mc setToRecipients:toRecipents];
//    
//    // Determine the file name and extension
//    NSArray *filepart = [fileAtPath componentsSeparatedByString:@"."];
//    NSString *filename = [filepart objectAtIndex:0];
//    NSString *extension = [filepart objectAtIndex:1];
//    
//    // Get the resource path and read the file using NSData
////    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
//    NSData *fileData = [NSData dataWithContentsOfFile:fileAtPath];
//    
//    // Determine the MIME type
//    NSString *mimeType;
//    if ([extension isEqualToString:@"jpg"]) {
//        mimeType = @"image/jpeg";
//    } else if ([extension isEqualToString:@"png"]) {
//        mimeType = @"image/png";
//    } else if ([extension isEqualToString:@"doc"]) {
//        mimeType = @"application/msword";
//    } else if ([extension isEqualToString:@"ppt"]) {
//        mimeType = @"application/vnd.ms-powerpoint";
//    } else if ([extension isEqualToString:@"html"]) {
//        mimeType = @"text/html";
//    } else if ([extension isEqualToString:@"pdf"]) {
//        mimeType = @"application/pdf";
//    }
//    
//    // Add attachment
//    [mc addAttachmentData:fileData mimeType:mimeType fileName:filename];
//    
//    // Present mail view controller on screen
//    [self presentViewController:mc animated:YES completion:NULL];
//    
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void) createExcel
{
//    NSArray *firstArray, *secondArray, *thirdArray;
//    NSMutableString *csv = [NSMutableString stringWithString:@"Name,Date,Miles"];
//    
//    NSUInteger count = [firstArray count];
//    // provided all arrays are of the same length
//    for (NSUInteger i=0; i<count; i++ ) {
//        [csv appendFormat:@"\n\"%@\",%@,\"%d\"",
//         [firstArray objectAtIndex:i],
//         [secondArray objectAtIndex:i],
//         [[thirdArray objectAtIndex:i] integerValue]
//         ];
//        // instead of integerValue may be used intValue or other, it depends how array was created
//    }
//    NSString *yourFileName = @"your filename";
//    NSError *error;
//    BOOL res = [csv writeToFile:yourFileName atomically:YES encoding:NSUTF8StringEncoding error:&error];
//    
//    if (!res) {
//        NSLog(@"Error %@ while writing to file %@", [error localizedDescription], yourFileName );
//    }
}

//-(void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
//{
//    // Creates a mutable data object for updating with binary data, like a byte array
//    NSMutableData *pdfData = [NSMutableData data];
//    
//    // Points the pdf converter to the mutable data object and to the UIView to be converted
//    UIGraphicsBeginPDFContextToData(pdfData, mainView.bounds, nil);
//    UIGraphicsBeginPDFPage();
//    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
//    
//    
//    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
//    
//    [mainView.layer renderInContext:pdfContext];
//    
//    // remove PDF rendering context
//    UIGraphicsEndPDFContext();
//    
//    // Retrieves the document directories from the iOS device
//    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
//    
//    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
//    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
//    
//    // instructs the mutable data object to write its context to a file on disk
//    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
//    //    NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
//}

- (IBAction)Action_Switch_isCora:(id)sender
{
    [singletonAdmin setIsCora:self.Switch_isCora.on];
}
- (IBAction)Action_Button_Log:(id)sender
{
    self.TextView_Output.text = [singletonAdmin getLogs];
}

- (IBAction)Action_Button_Castiguri:(id)sender
{
    NSDate * date = self.DatePicker_Date.date;
    
    self.TextView_Output.text = [singletonAdmin getWinnersOnDate:date]; //[singletonAdmin getTodayWinners];
    [self sendEmailWithText:self.TextView_Output.text];
}
- (IBAction)Action_Button_Reset:(id)sender
{
    //
    self.TextField_Prize1.text = @"0";
    self.TextField_Prize2.text = @"0";
    self.TextField_Prize3.text = @"0";
    
    self.TextField_NumeMagazin.text = @"";
    
    self.TextView_Output.text = @"Reset";
    
    [singletonAdmin deleteAllEntities];
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    
}
- (IBAction)Action_Button_CastiguriTotale:(id)sender
{
    self.TextView_Output.text = [singletonAdmin getAllWinners];
    [self sendEmailWithText:self.TextView_Output.text];
}

-(void) sendEmailWithText:(NSString *) msg
{
    NSArray *toRecipents = [NSArray arrayWithObject:@"victor.furtuna@mediascope.ro"];
    MFMailComposeViewController *mail=[[MFMailComposeViewController alloc]init];
    
    
    if(mail == nil)
    {
        UIAlertView *objAlert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Nu este nici o adresa de email asociata acestui Device! Adaugati un cont de mail. Settings -> Mail-> Add Account" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close",nil];
        [objAlert show];
        
        return;
    }
    
    mail.mailComposeDelegate=self;
    NSString * emailTitle = [NSString stringWithFormat:@"Rezultate pentru %@",singletonAdmin.getStoreName];
    [mail setSubject:emailTitle];//@"Email with attached txt"];
    [mail setToRecipients:toRecipents];
    NSString * body = msg;
    [mail setMessageBody:body isHTML:NO];
    [self presentModalViewController:mail animated:YES];
}

@end
