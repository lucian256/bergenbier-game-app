//
//  Player.h
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedPlayer : NSObject

@property (strong, nonatomic) NSString * datanasterii;
@property (strong, nonatomic) NSString * nume;
@property (strong, nonatomic) NSString * prenume;
@property (strong, nonatomic) NSString * email;
@property (strong, nonatomic) NSString * telefon;
@property (strong, nonatomic) NSString * numarbon;
@property (strong, nonatomic) NSString * participaextragere;
@property (strong, nonatomic) NSString * datacurenta;
@property (strong, nonatomic) NSString * premiu;

@end
