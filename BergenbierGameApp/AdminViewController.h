//
//  AdminViewController.h
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface AdminViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *TextField_Prize1;
@property (strong, nonatomic) IBOutlet UITextField *TextField_Prize2;
@property (strong, nonatomic) IBOutlet UITextField *TextField_Prize3;
- (IBAction)Action_Button_BackSave:(id)sender;
- (IBAction)Action_Button_Email:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *TextView_Output;
- (IBAction)Action_Switch_isCora:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *Switch_isCora;
- (IBAction)Action_Button_Log:(id)sender;
- (IBAction)Action_Button_Castiguri:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *TextField_NumeMagazin;
- (IBAction)Action_Button_Reset:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *Button_Reset;
@property (strong, nonatomic) IBOutlet UIButton *Button_Log;

- (IBAction)Action_Button_CastiguriTotale:(id)sender;
@property (strong, nonatomic) IBOutlet UIDatePicker *DatePicker_Date;
@end
