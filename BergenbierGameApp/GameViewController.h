//
//  GameViewController.h
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"


#import <DNDDragAndDrop/DNDDragAndDrop.h>
#import "DragView.h"

#import "DDHTimerControl.h"

#import "CircularTimerView.h"

#import "RZTweenAnimator.h"
#import "RZTweenSpirit.h"

#import "AVFoundation/AVAudioPlayer.h"
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>

@interface GameViewController : UIViewController
#pragma Drag
@property (nonatomic, weak) IBOutlet UIView *dragSourceView;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *dropTargetViews;
@property (nonatomic, strong) IBOutlet DNDDragAndDropController *dragAndDropController;
#pragma endDrag

- (IBAction)Action_Button_Back:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *View_Clock;
//time1
@property (nonatomic, strong) NSDate *endDate;

@property (nonatomic, strong) CircularTimerView *circularTimer;

@property (nonatomic) BOOL gameEnd;

@property (strong, nonatomic) IBOutlet UIView *ViewPrize;
@property (strong, nonatomic) IBOutlet UIImageView *ImageView_Prize;
- (IBAction)Action_Button_Restart:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *ImageView_Sup;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *Constraint_supTop;

@property (nonatomic, strong) RZTweenAnimator *firstViewAnimator;

@property (nonatomic, strong) AVAudioPlayer * audioPlayer;

@end
