//
//  Player.m
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "SelectedPlayer.h"

@implementation SelectedPlayer

- (instancetype)init
{
    self.datanasterii = @"";
    self.nume = @"";
    self.prenume = @"";
    self.email = @"";
    self.telefon = @"";
    self.numarbon = @"";
    self.participaextragere = @"0";
    
    self.premiu = @"";
    self.datacurenta = @"";
    
    return self;
}
@end
