//
//  ViewController.m
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface ViewController () <DNDDragSourceDelegate, DNDDropTargetDelegate>
@end

int countx =3;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Drag
    self.dragAndDropController = [[DNDDragAndDropController alloc] init];
    
    [self.dragAndDropController registerDragSource:self.dragSourceView withDelegate:self];
    
    
    for (UIView *dropTargetView in self.dropTargetViews)
    {
        dropTargetView.layer.borderColor = [[UIColor blueColor] CGColor];
        
        dropTargetView.backgroundColor = [UIColor redColor];
        
        dropTargetView.layer.borderWidth = 4.0f;
        
        UILabel *label = (UILabel*)[dropTargetView viewWithTag:101];
        
        label.text = @"0";
        
        [self.dragAndDropController registerDropTarget:dropTargetView withDelegate:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Drag Source Delegate

- (UIView *)draggingViewForDragOperation:(DNDDragOperation *)operation
{
        if(countx == 0)
        {
            return nil;
        }

    
    
    NSLog(@"Initial : %d now %d",countx,countx-1);
    countx --;
    UIView *dragView = [[DragView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 150.0f, 75.0f)];
    dragView.alpha = 0.0f;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.alpha = 1.0f;
    }];
    return dragView;
}

- (void)dragOperationWillCancel:(DNDDragOperation *)operation
{
    
    // Drag operation will cancel
    
    [operation removeDraggingViewAnimatedWithDuration:0.2 animations:^(UIView *draggingView) {
        draggingView.alpha = 0.0f;
        draggingView.center = [operation convertPoint:self.dragSourceView.center fromView:self.view];
    }];
    
    countx ++;
    
}


#pragma mark - Drop Target Delegate

- (void)dragOperation:(DNDDragOperation *)operation didDropInDropTarget:(UIView *)target
{
    //Drop End
    target.backgroundColor = operation.draggingView.backgroundColor;
    target.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    
    UILabel *label = (UILabel*)[target viewWithTag:101];
    label.text = @"1";
    
    //count --;
    
    
    
    [self.dragAndDropController unregisterDropTarget:target];
    
//    if(count == 0)
//    {
//        [self.dragAndDropController unregisterDragSource:self.dragSourceView];
//    }
}





- (void)dragOperation:(DNDDragOperation *)operation didEnterDropTarget:(UIView *)target
{
    //Drop Hover
    target.layer.borderColor = [operation.draggingView.backgroundColor CGColor];
}

- (void)dragOperation:(DNDDragOperation *)operation didLeaveDropTarget:(UIView *)target
{
    //Drop Hover
    target.layer.borderColor = [[UIColor whiteColor] CGColor];
}

#pragma Stuff




@end
