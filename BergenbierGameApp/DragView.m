//
//  DragView.m
//  BergenbierGameApp
//
//  Created by Alex on 13/05/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "DragView.h"

@implementation DragView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
        self.backgroundColor = [UIColor clearColor];
        //self.layer.borderColor = [[UIColor whiteColor] CGColor];
        //self.layer.borderWidth = 2.0f;
        //self.layer.cornerRadius = 5.0f;
        self.userInteractionEnabled = YES;
        self.clipsToBounds = YES;
        
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:frame];
        
        imageView.image = [UIImage imageNamed:@"TShirt1"];
        
        [self addSubview:imageView];
        
    }
    return self;
}

@end
